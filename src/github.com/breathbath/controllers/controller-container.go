package controllers
import (
	"net/http"
)

type ControllerContainer struct {
	ControllersCollection []Controller
}

func NewControllerContainer() *ControllerContainer {
	return &ControllerContainer{ControllersCollection:[]Controller{}}
}

func (cc *ControllerContainer) AddNewByParams(RouteName string, RouteMethod string, RoutePattern string, handler http.HandlerFunc) {
	controller := Controller{
		Name:RouteName,
		Method: RouteMethod,
		Pattern: RoutePattern,
		HandlerFunc: handler,
	}
	cc.AddNew(controller)
}

func (cc *ControllerContainer) AddNew(c Controller) {
	cc.ControllersCollection = append(cc.ControllersCollection, c)
}

func (cc *ControllerContainer) Merge(c ControllerContainer) {
	for _, controllr := range c.ControllersCollection {
		cc.AddNew(controllr)
	}
}

type ControllerContainerFactory interface{
	CreateControllerContainer() *ControllerContainer
}