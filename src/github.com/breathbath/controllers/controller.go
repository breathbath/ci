package controllers
import (
	"net/http"
)

type Controller struct {
	Name        string
	Method      string
	Pattern     string
	HandlerFunc http.HandlerFunc
}