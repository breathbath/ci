package router

import (
	"github.com/gorilla/mux"
	"net/http"
	"github.com/breathbath/controllers"
	"time"
	"log"
)

func NewMuxRouter(cc controllers.ControllerContainer) *mux.Router {
	mRouter := mux.NewRouter().StrictSlash(true)
	for _, controllr := range cc.ControllersCollection {
		var handler http.Handler
		handler = controllr.HandlerFunc
		handler = createLoggableHandler(handler, controllr.Name)
		mRouter.
			Methods(controllr.Method).
			Path(controllr.Pattern).
			Name(controllr.Name).
			Handler(handler)
	}

	return mRouter
}

func createLoggableHandler(controller http.Handler, name string) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		start := time.Now()

		controller.ServeHTTP(w, r)

		log.Printf(
			"%s\t%s\t%s\t%s",
			r.Method,
			r.RequestURI,
			name,
			time.Since(start),
		)
	})
}