package commands
import (
	"os/exec"
	"github.com/breathbath/console/lib/io"
	"github.com/breathbath/console/lib/command"
)

const PWD_NAME  = "pwd"

type Pwd struct{
	command.Command
}

func NewPwdCommand() command.Command {
	return &Pwd{}
}

func (pwd Pwd) Execute(request *io.Request, response *io.Response) {
	out, err := exec.Command("sh", "-c", "pwd").Output()
	if err != nil {
		response.AddError(err)
	} else {
		response.AddMessage(string(out))
	}
}

func (pwd Pwd) GetPackageName() string {
	return "system"
}

func (pwd Pwd) GetName() string {
	return PWD_NAME
}

func (pwd Pwd) GetDescription() string {
	return "Shows current working directory"
}

func (pwd Pwd) Validate(request *io.Request) error {
	return nil
}