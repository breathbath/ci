package commands
import (
	"github.com/breathbath/console/lib/command"
)

func BuildCommandsLocator() *command.CommandLocator {
	locator := command.NewCommandLocator()
	locator.AddCommand(NewPwdCommand())
	locator.AddCommand(NewDrawPyramidCommand())
	locator.AddCommand(NewFibonacci())

	return locator
}