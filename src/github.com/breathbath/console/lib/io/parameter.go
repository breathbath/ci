package io

type Parameter struct {
	ParamName string
	DefaultValue string
	IsRequired bool
	Description string
	Value string
}