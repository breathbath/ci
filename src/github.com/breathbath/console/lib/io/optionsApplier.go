package io

type OptionsApplier struct{}

func NewOptionsApplier() *OptionsApplier {
	return &OptionsApplier{}
}

func (this *OptionsApplier) Apply(request *Request, response *Response) {
	var ok bool
	_, ok = request.GetOptionString("v")
	if ok {
		response.SetVerbose(true)
	}

	_, ok = request.GetOptionString("n")
	if ok {
		response.SetVerbose(false)
	}
}