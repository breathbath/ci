package command

type CommandLocator struct {
	Commands map[string]Command
}

func NewCommandLocator() *CommandLocator {
	return &CommandLocator{Commands:make(map[string]Command)}
}

func (this *CommandLocator) AddCommand(command Command) {
	this.Commands[command.GetName()] = command
}

func (this *CommandLocator) GetCommandByName(name string) (Command, bool) {
	i, ok := this.Commands[name]
	return i, ok
}