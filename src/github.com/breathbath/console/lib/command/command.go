package command

import (
	"github.com/breathbath/console/lib/io"
)

type Command interface {
	Execute(request *io.Request, response *io.Response)
	GetName() string
	GetPackageName() string
	GetDescription() string
	Validate(request *io.Request) error
}