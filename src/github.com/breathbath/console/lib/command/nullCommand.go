package command
import "github.com/breathbath/console/lib/io"

type NullCommand struct{}

func (this *NullCommand) Validate(request *io.Request) error {
	return nil
}