package command
import (
	"github.com/breathbath/console/lib/io"
	"errors"
	"strings"
	"github.com/breathbath/console/lib/validators"
)

type ParametrisedCommand struct {
	Params     []io.Parameter
	Options io.OptionsCollection
	Validators map[string][]validators.ParameterValidator
}

func (this *ParametrisedCommand) GetParam(paramName string, request *io.Request) (io.Parameter) {
	registeredParam := this.getRegisteredParamByName(paramName)
	if &registeredParam == nil {
		return registeredParam
	}

	paramValue, ok := request.GetParameterString(paramName)
	if ok {
		registeredParam.Value = paramValue
	} else {
		registeredParam.Value = registeredParam.DefaultValue
	}

	return registeredParam
}

func (this *ParametrisedCommand) HasOption(name string, request *io.Request) bool {
	_, ok := request.GetOptionString(name)
	return ok
}

func (this *ParametrisedCommand) getRegisteredParamByName(name string) io.Parameter {
	for _, registeredParam := range this.Params {
		if registeredParam.ParamName == name {
			return registeredParam
		}
	}
	return io.Parameter{}
}

func (this *ParametrisedCommand) AddParameter(param io.Parameter) {
	if this.Params == nil {
		this.Params = []io.Parameter{}
	}
	this.Params = append(this.Params, param)
}

func (this *ParametrisedCommand) AddOption(option io.Option) {
	if this.Options == nil {
		this.Options = []io.Option{}
	}
	this.Options = append(this.Options, option)
}

func (this *ParametrisedCommand) AddParameterValidator(parameter *io.Parameter, validator validators.ParameterValidator) {
	if this.Validators == nil {
		this.Validators = make (map[string][]validators.ParameterValidator)
	}
	currSlice := this.Validators[parameter.ParamName]
	currSlice = append(currSlice, validator)

	this.Validators[parameter.ParamName] = currSlice
}

func (this *ParametrisedCommand) hasValidators() bool {
	return this.Validators != nil
}

func (this *ParametrisedCommand) validateParameters(request *io.Request) error {
	missingRequiredParams := []string{}
	for _, param := range this.Params {
		paramValue, found := request.GetParameterString(param.ParamName)

		if !found && param.IsRequired {
			missingRequiredParams = append(missingRequiredParams, param.ParamName)
		}

		if this.hasValidators() {
			paramValidators, ok := this.Validators[param.ParamName]
			if ok {
				for _, paramValidator := range paramValidators {
					err := paramValidator.Validate(paramValue, param, *request)
					if err != nil {
						return err
					}
				}
			}
		}
	}
	if len(missingRequiredParams) > 0 {
		return errors.New("Missing required command param(s): " + strings.Join(missingRequiredParams, ","))
	}
	return nil
}

func (this ParametrisedCommand) Validate(request *io.Request) error {
	if len(this.Params) > 0 {
		return this.validateParameters(request)
	}
	return nil
}

func (this *ParametrisedCommand) GetParams() []io.Parameter {
	if this.Params == nil {
		return []io.Parameter{}
	}
	return this.Params
}

func (this *ParametrisedCommand) GetOptions() io.OptionsCollection {
	if this.Options == nil {
		return io.OptionsCollection{}
	}
	return this.Options
}