package command

import (
	"github.com/breathbath/console/lib/io"
	"github.com/breathbath/console/lib/validators"
)

type CommandWithParams interface {
	GetParam(paramName string, request *io.Request) (io.Parameter)
	AddParameter(param io.Parameter)
	AddParameterValidator(parameter *io.Parameter, validator validators.ParameterValidator)
	GetParams() []io.Parameter
	HasOption(name string, request *io.Request) bool
	AddOption(option io.Option)
	GetOptions() io.OptionsCollection
}