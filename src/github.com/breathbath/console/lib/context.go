package lib
import "github.com/breathbath/console/lib/command"

type Context struct{
	Arguments      []string
	CommandLocator *command.CommandLocator
	LogMethod      func(errors [] error)
	OutputMethod   func(message string)
}