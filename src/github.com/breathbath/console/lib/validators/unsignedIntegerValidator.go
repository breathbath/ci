package validators
import (
	"github.com/breathbath/console/lib/io"
	"errors"
	"fmt"
	"strconv"
)

type UnsignedIntegerValidator struct{}

func (this UnsignedIntegerValidator) Validate(value string, parameter io.Parameter, request io.Request) error {
	paramName := parameter.ParamName
	if value == "" {
		return errors.New(fmt.Sprintf("Missing param %s", paramName))
	}

	val, err := strconv.ParseUint(value, 0, 0)
	if err != nil {
		return errors.New("Cannot convert value " + paramName + " to int")
	}

	if val < 1 {
		return errors.New(fmt.Sprintf("Param \"%s\" should be unsigned ", paramName))
	}
	return nil
}