package validators
import (
	"github.com/breathbath/console/lib/io"
	"errors"
	"strings"
)

type RequestValidator struct {
	commandOptions io.OptionsCollection
}

func NewRequestValidator(inputCommandOptions io.OptionsCollection) *RequestValidator {
	return &RequestValidator{commandOptions:inputCommandOptions}
}

func (this *RequestValidator) ValidateRequest(request *io.Request) error {
	allKnownOptions := io.NewGeneralOptions()
	allKnownOptions = append(allKnownOptions, this.commandOptions ...)

	unknownOptions := []string{}
	requestOptions := request.Options
	for _, v := range requestOptions {
		if !allKnownOptions.HasOption(v) {
			unknownOptions = append(unknownOptions, v)
		}
	}

	if len(unknownOptions) != 0 {
		return errors.New("Unknown option(s): " + strings.Join(unknownOptions, ","))
	}

	return nil
}

func (this *RequestValidator) GetUnknownOptions(request *io.Request, knownParams map[string]int) []string {
	unknownParams := []string{}
	allParams := request.Options
	for _, v := range allParams {
		_, ok := knownParams[v]
		if !ok {
			unknownParams = append(unknownParams, v)
		}
	}
	return unknownParams
}