package validators
import "github.com/breathbath/console/lib/io"

type ParameterValidator interface {
	Validate(value string, parameter io.Parameter, request io.Request) error
}