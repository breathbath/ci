package lib
import (
	"github.com/breathbath/console/lib/io"
	"github.com/breathbath/console/lib/validators"
	"github.com/breathbath/console/lib/command"
	"github.com/breathbath/console/lib/help"
	"errors"
)

func createIo(arguments []string) (*io.Request, *io.Response) {
	request := io.NewRequestFromArgs(arguments)
	response := io.NewResponse()

	optionsApplier := io.NewOptionsApplier()
	optionsApplier.Apply(request, response)

	return request, response
}

func validateRequestInCommand(request *io.Request, response *io.Response, curCommand command.Command) error {
	if curCommand == nil {
		return errors.New("Unknown command " + request.CommandName)
	}

	if paramCommand, ok := curCommand.(command.CommandWithParams); ok {
		requestValidator := validators.NewRequestValidator(paramCommand.GetOptions())
		err := requestValidator.ValidateRequest(request)
		if err != nil {
			response.AddError(err)
			return err
		}
	}

	err := curCommand.Validate(request)
	if err != nil {
		response.AddError(err)
	}

	return err
}

func addHelpCommand(locator *command.CommandLocator) {
	locator.AddCommand(help.NewHelpCommand(locator))
}

func findCommand(request *io.Request, response *io.Response, locator *command.CommandLocator) command.Command {
	dispatcher := command.NewDispatcher(locator)
	return dispatcher.Dispatch(request, response)
}

func logResponseErrors(context Context, response *io.Response) {
	if response.HasErrors() {
		context.LogMethod(response.Errors)
	}
}

func outputErrorMessages(response *io.Response, context Context) {
	if response.Verbosity && len(response.Messages) > 0 {
		for _, msg := range response.Messages {
			context.OutputMethod(msg)
		}
	}
}

func Execute(context Context) {
	request, response := createIo(context.Arguments)
	commandLocator := context.CommandLocator
	addHelpCommand(commandLocator)

	command := findCommand(request, response, commandLocator)

	if validateRequestInCommand(request, response, command) == nil {
		command.Execute(request, response)
	}

	logResponseErrors(context, response)
	outputErrorMessages(response, context)
}