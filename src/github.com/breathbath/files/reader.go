package files

import (
	"path/filepath"
	"os"
)

func GetCurrentPath() (string, error) {
	return os.Getwd()
}

func GetConfigPath() (string, error) {
	currPath, err := GetCurrentPath()
	if err != nil {
		return currPath, err
	}
	currPath = filepath.Join(currPath, "config")
	return currPath, err
}

func Join(elem ...string) string {
	return filepath.Join(elem...)
}