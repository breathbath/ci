package dependencies
import (
	"github.com/breathbath/controllers"
	"github.com/gorilla/mux"
	"github.com/breathbath/app/appcontrollers"
	"github.com/breathbath/router"
)

type DependencyContainer struct {
}

func NewDependencyContainer() DependencyContainer{
	return DependencyContainer{}
}

func (dc DependencyContainer) CreateMuxRouter() *mux.Router {
	return router.NewMuxRouter(dc.CollectAllControllers())
}

func (dc DependencyContainer) CollectAllControllers() controllers.ControllerContainer {
	allControllerFactories := make([] controllers.ControllerContainerFactory, 2)

	allControllerFactories[0] = appcontrollers.CreateHomeControllerFactory()
	allControllerFactories[1] = appcontrollers.CreateTasksControllerFactory()

	return dc.extractControllersFromFactories(allControllerFactories)
}

func (dc DependencyContainer) extractControllersFromFactories(controllerFactories [] controllers.ControllerContainerFactory) controllers.ControllerContainer {
	container := controllers.NewControllerContainer()
	for _, cFactory := range controllerFactories {
		cont := cFactory.CreateControllerContainer()
		container.Merge(*cont)
	}
	return *container
}