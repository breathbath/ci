package appcontrollers
import (
	"net/http"
	"fmt"
	"github.com/breathbath/controllers"
)

type HomeControllerFactory struct {}

func CreateHomeControllerFactory() HomeControllerFactory {
	return HomeControllerFactory {}
}

func (hcf HomeControllerFactory) CreateControllerContainer() *controllers.ControllerContainer {
	homeController := controllers.Controller{
		"Index",
		"GET",
		"/",
		func (w http.ResponseWriter, r *http.Request) {
			fmt.Fprintln(w, "Home controller index action")
		},
	}
	controllerContainer := controllers.NewControllerContainer()
	controllerContainer.AddNew(homeController)

	return controllerContainer
}