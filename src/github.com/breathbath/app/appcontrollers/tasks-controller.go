package appcontrollers
import "net/http"
import (
	"encoding/json"
	"github.com/gorilla/mux"
	"fmt"
	"io/ioutil"
	"io"
	"github.com/breathbath/controllers"
)

type TasksControllerFactory struct {
	controllersContainer *controllers.ControllerContainer
}

func CreateTasksControllerFactory() TasksControllerFactory {
	return TasksControllerFactory {controllersContainer: controllers.NewControllerContainer()}
}

func (ncf TasksControllerFactory) CreateControllerContainer() *controllers.ControllerContainer {
	ncf.controllersContainer.AddNewByParams(
		"TasksIndex",
		"GET",
		"/tasks",
		func (w http.ResponseWriter, r *http.Request) {
			IndexAction(w, r)
		},
	)
	ncf.controllersContainer.AddNewByParams(
		"TaskShow",
		"GET",
		"/tasks/{taskId}",
		func (w http.ResponseWriter, r *http.Request) {
			ViewAction(w, r)
		},
	)
	ncf.controllersContainer.AddNewByParams(
		"TasksCreate",
		"POST",
		"/tasks",
		func (w http.ResponseWriter, r *http.Request) {
			CreateAction(w, r)
		},
	)
	return ncf.controllersContainer
}

func IndexAction(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)

	if err := json.NewEncoder(w).Encode("[]"); err != nil {
		panic(err)
	}
}

func ViewAction(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	newsId,_ := vars["taskId"]
	taskStr := fmt.Sprintf("Task show: %s", newsId)
	fmt.Fprintln(w, taskStr)
}

func CreateAction(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(io.LimitReader(r.Body, 1048576))
	fmt.Println(body)
	if err != nil {
		panic(err)
	}
	if err := r.Body.Close(); err != nil {
		panic(err)
	}
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusCreated)
}