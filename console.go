package main

import (
"os"
"log"
"fmt"
	"github.com/breathbath/console/commands"
"github.com/breathbath/console/lib"
)

func main() {
	context := lib.Context{
		Arguments:os.Args,
		CommandLocator: commands.BuildCommandsLocator(),
		LogMethod: func(errors [] error) {
			log.Fatal(errors)
		},
		OutputMethod: func(message string) {
			fmt.Println(message)
		},
	}
	lib.Execute(context)
}