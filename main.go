package main
import (
	"net/http"
	"github.com/breathbath/app/dependencies"
)

func main() {
	dependencyContainer := dependencies.NewDependencyContainer()
	http.ListenAndServe(":8080", dependencyContainer.CreateMuxRouter())
}